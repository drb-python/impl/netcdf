from drb.exceptions.core import DrbException, DrbFactoryException


class DrbNetcdfNodeException(DrbException):
    pass


class DrbNetcdfNodeFactoryException(DrbFactoryException):
    pass
