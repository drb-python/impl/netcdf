.. _install:

Installation of netcdf implementation
====================================
To include this module into your project, the ``drb-driver-netcdf`` module shall be referenced into requirements.txt file,
or the following pip line can be run:

.. code-block::

    pip install drb-driver-netcdf
