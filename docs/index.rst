===================
Data Request Broker
===================
---------------------------------
NETCDF driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-netcdf/month
    :target: https://pepy.tech/project/drb-driver-netcdf
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-netcdf.svg
    :target: https://pypi.org/project/drb-driver-netcdf/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-netcdf.svg
    :target: https://pypi.org/project/drb-driver-netcdf/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-netcdf.svg
    :target: https://pypi.org/project/drb-driver-netcdf/
    :alt: Python Version Support Badge

-------------------

This module implements netcdf format access with DRB data model.
It is able to navigates among the netcdf contents.

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

