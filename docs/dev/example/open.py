import netCDF4

from drb.drivers.netcdf import DrbNetcdfFactory
from drb.drivers.file import DrbFileFactory

path_file = 'file/netcdf.nc'

node_file = DrbFileFactory().create(path_file)
node = DrbNetcdfFactory().create(node_file)

# Get the first Group node.
node[0]

# Get a specific attribute of the first group.
node[0] @ 'attributes_name'

# Get all the attributes
node[0].attribute_names()

# Get the value of the group.
node[0].value

# Retrieve a dataset
netcdf_dataset = node[0]['data_set_name'].get_impl(netCDF4.Dataset)

