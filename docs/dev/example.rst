.. _example:

Examples
=========

Open and read a netcdf file
-----------------------------
.. literalinclude:: example/open.py
    :language: python
